Documentation!
===================================

This Epics IOC was created to communicate via GPIB to the Hewlett Packard 3458A, now `Keysight <https://www.keysight.com/us/en/product/3458A/digital-multimeter-8-5-digit.html>`_.

Check out the :doc:`usage` section for further information.

.. note::

   This project is under active development.

Contents
--------

.. toctree:: 
   installation
   usage
   records
   protocol


