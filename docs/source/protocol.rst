Protocol
==========

The protocol file describes the communication via GPIB with the hp3458A. It encompasses general settings such as timeouts and command terminators.

The communication protocol, based on the stream-device of EPICS Control Systems, facilitates the interaction with the hp3458A. It distinguishes between Decimal functions (setDec and getDec), Float functions (setFlt and getFlt), and String functions (setStr and getStr) for sending and receiving different data types.

Moreover, the protocol includes provisions for reading arrays using getArray. In the EPICS record, this feature is employed to retrieve settings such as NRDGS <n>,<event> (refer to the manual for details).

The send command serves as the primary means for users to control the device by sending custom commands via this field and subsequently monitoring the device's status through built-in records.

The query command provides users with the flexibility for custom commands and queries that may not be available in the IOC and the UI.

The single ARM command enables triggering the device. Refer to the manual for detailed information.

The rmem command will read the memory if not empty and return an array.

.. note::
    The OutTerminators/InTerminators can be defined either at the stream device or at the "/opt/epics/ioc/hp3458a-gpib/iocBoot/iocHHS_HP3458A_GPIB/st.cmd" command using asynOctetSetOutputEos.

.. literalinclude:: ../../protocols/hp3458a.proto
