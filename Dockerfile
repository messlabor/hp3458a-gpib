## set base image with generic keithley
FROM registry.hzdr.de/hzb/epics/ioc/images/adsimdetectorgenericimage:latest AS base

## set default shell for next commands
SHELL ["/bin/bash", "-c"]

# install packages 
RUN apt update -y && \
    apt upgrade -y && \
    apt-get install -y --no-install-recommends \
    ca-certificates \
    libntirpc-dev libtirpc3 \
    build-essential \
    busybox \
    git \
    python3-minimal \
    python3-pip \
    python3-venv \
    re2c \
    rsync \
    ssh-client \
    dkms \
    lsb-release \
    lsb-core \
    libgconf-2-4 \
    libgtk-3-0 \
    libasound2 \
    libcanberra-gtk-module \
    libcanberra-gtk3-module \
    x11-apps \
    software-properties-common \
    gawk \
    iptables \
    libxss1 \
    libappindicator1 \
    libunwind8 \
    libx11-xcb1 \
    pandoc


RUN pip install pyvisa pyvisa-py gpib-ctypes psutil zeroconf matplotlib pyepics pandoc numpy

# copy files
# COPY iolibrariessuite-installer_20.1.29718.0.run /opt
# COPY iokerneldrivers-installer_20.1.29718.0.run /opt 
# COPY configure/RELEASE.local /opt/epics/support

# prepare environment
RUN export SUPPORT=/opt/epics/support && \
    mkdir /opt/epics/ioc && \
    export IOCS=/opt/epics/ioc && \
    git config --global advice.detachedHead false && \
    echo "export LC_ALL=C" >> ~/.bashrc

# clone ioc
RUN cd /opt/epics/ioc && \
    git clone --depth 1 --recursive https://codebase.helmholtz.cloud/messlabor/helmholtzcoil/hp3458a-gpib.git && \
    cp /opt/epics/ioc/hp3458a-gpib/configure/RELEASE.local ${SUPPORT} && \
    cp /opt/epics/ioc/hp3458a-gpib/configure/RELEASE.local /opt/epics/ioc/RELEASE.local

# # install iols
RUN chmod +x /opt/epics/ioc/hp3458a-gpib/iolibrariessuite-installer_20.1.29718.0.run && \
    # sudo chmod +x /opt/epics/ioc/hp3458a-gpib/iokerneldrivers-installer_20.1.29718.0.run  && \ 
    /opt/epics/ioc/hp3458a-gpib/iolibrariessuite-installer_20.1.29718.0.run --mode unattended
    # sudo /opt/epics/ioc/hp3458a-gpib/iokerneldrivers-installer_20.1.29718.0.run --mode unattended

# install pcre
RUN git clone --depth 1 --recursive --branch R8-44 https://github.com/chrschroeder/pcre.git ${SUPPORT}/pcre
RUN make -C ${SUPPORT}/pcre -j $(nproc)

# install stream
RUN git clone --depth 1 --recursive --branch 2.8.24 https://github.com/paulscherrerinstitute/StreamDevice.git ${SUPPORT}/stream
RUN make -C ${SUPPORT}/stream -j $(nproc)

# install visa epics
RUN git clone --depth 1 --recursive https://github.com/ISISComputingGroup/EPICS-VISA.git ${SUPPORT}/visa && \
    sed -i 's#\$(APPNAME)_SYS_LIBS_Linux += visa#\$(APPNAME)_SYS_LIBS_Linux += iovisa#' ${SUPPORT}/visa/visa_lib.mak && \
    sed -i 's#USR_INCLUDES += -I/usr/include/ni-visa#USR_INCLUDES += -I/opt/keysight/iolibs/include#' ${SUPPORT}/visa/VISAdrvApp/src/Makefile && \
    sed -i '/\$(APPNAME)_DBD += VISAdrv.dbd/a \$(APPNAME)_DBD += calc.dbd' ${SUPPORT}/visa/VISAdrvTestApp/src/build.mak && \
    sed -i '/\$(APPNAME)_LIBS += stream VISAdrv asyn/s/$/ calc/' ${SUPPORT}/visa/VISAdrvTestApp/src/build.mak &&\
    sed -i 's/^SSCAN =/#SSCAN =/' ${SUPPORT}/visa/configure/RELEASE && \
    make -C ${SUPPORT}/visa clean install
