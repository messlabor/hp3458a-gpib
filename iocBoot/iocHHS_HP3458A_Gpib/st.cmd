#!../../bin/windows-x64-mingw/HHS_HP3458A_Gpib

#- You may have to change HHS_HP3458A_Gpib to something else
#- everywhere it appears in this file

< envPaths

cd "${TOP}"


epicsEnvSet("STREAM_PROTOCOL_PATH", "${TOP}/protocols")

epicsEnvSet("EPICS_CA_AUTO_ADDR_LIST", "NO")
epicsEnvSet("EPICS_CA_ADDR_LIST", "localhost")
# epicsEnvSet("EPICS_CA_SERVER_PORT", "44619")

## Register all support components
dbLoadDatabase "dbd/HHS_HP3458A_Gpib.dbd"
HHS_HP3458A_Gpib_registerRecordDeviceDriver pdbbase

## Register serial port
drvAsynVISAPortConfigure("L0", "GPIB0::22::INSTR")
asynOctetSetOutputEos("L0",0,"\r\n")
asynOctetSetInputEos("L0",0,"\r") # always with \r

## Load record instances
dbLoadRecords("db/HHS_HP3458A_Gpib.db","PORT=L0, P=DVM")

cd "${TOP}/iocBoot/${IOC}"
iocInit

## Start any sequence programs
#seq sncxxx,"user=a3558"
