#include <stdio.h>
#include <dbDefs.h>
#include <subRecord.h>
#include <aSubRecord.h>
#include <registryFunction.h>
#include <epicsExport.h>
#include <string.h>

// Function to get the corresponding name for a given number
const char* getMathName(int number) {
    switch (number) {
        case 0: return "OFF";
        case 1: return "CONT";
        case 3: return "CTHRM";
        case 4: return "DB";
        case 5: return "DBM";
        case 6: return "FILTER";
        case 8: return "FTHRM";
        case 9: return "NULL";
        case 10: return "PERC";
        case 11: return "PFAIL";
        case 12: return "RMS";
        case 13: return "SCALE";
        case 14: return "STAT";
        case 16: return "CTHRM2K";
        case 17: return "CTHRM10K";
        case 18: return "FTHRM2K";
        case 19: return "FTHRM10K";
        case 20: return "CRTD85";
        case 21: return "CRTD92";
        case 22: return "FRTD85";
        case 23: return "FRTD92";
        default: return "Unknown"; // Default case for unknown numbers
    }
}

static long mapMathName(aSubRecord *prec){
    
    int *a;
    const char* result;

    a = (int *)prec->a;  // Fix: Removed the dereference
    result = getMathName(*a);  // Fix: Cast prec->a to int
    strcpy((char *)prec->vala, result);

    return 0;
}

epicsRegisterFunction(mapMathName);