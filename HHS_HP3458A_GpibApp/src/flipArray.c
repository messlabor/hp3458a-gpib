

#include <stdio.h>
#include <dbDefs.h>
#include <subRecord.h>
#include <aSubRecord.h>
#include <aaiRecord.h>
#include <registryFunction.h>
#include <epicsExport.h>
#include <string.h>


const char* flipArray(aSubRecord *prec){

    const int n_size=(int)prec->noa;
    //printf("Number of arguments: %d\n", n_size);

    float *a;
    a = (float*)prec->a;

    float inverted[n_size];

    for (int i=0; i<n_size; i++){
        //printf("a[%d] = %f\n", i, a[i]);
        inverted[i] = a[n_size-i-1];
        //printf("inverted[%d] = %f\n", i, inverted[i]);
    }

    memcpy(prec->vala, inverted, sizeof(float)*n_size);

    return 0;

}

epicsRegisterFunction(flipArray);