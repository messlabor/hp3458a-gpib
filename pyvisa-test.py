
import pyvisa

path = "/opt/keysight/iolibs/bin/libautocf32.so"

rm = pyvisa.ResourceManager(path)
print(rm.list_resources())

# Send command "ID?" and query the response
instrument = rm.open_resource("GPIB0::1::INSTR")
instrument.write("ID?")
response = instrument.read()
print(response)