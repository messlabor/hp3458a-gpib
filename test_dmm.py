import sys

sys.path.append("/opt/epics/ioc/hp3458a-gpib")

from class_hp3458a import Hp3458a
from time import sleep
import datetime
import matplotlib.pyplot as plt

# Create the DVM Object
dmm = Hp3458a("DVM:", name = "dmm")
dmm.wait_for_connection()
dmm.reset.put(1) # reset the DVM
sleep(1)

# Arm Multimeter
run_measurement = "PRESET FAST;APER 8.0E-3;MFORMAT ASCII;MEM FIFO;TIMER 4.70e-5;RANGE 10.0;NRDGS 1000,AUTO;TARM SGL,1;OFORMAT ASCII;RMEM 1,1000,1"
dmm.send.put(run_measurement) # configure measurement
sleep(4.7e-5*1000+2) # wait for the measurement to complete

print("Measurement Done")
print("Reading Memory")
arr_dmm = dmm.read_memory()
print("Plot")
plt.plot(arr_dmm)
plt.xlabel('Measurement Number')
plt.ylabel('Voltage (V)')
plt.show()