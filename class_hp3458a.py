# reference: 
#   ophyd:  https://codebase.helmholtz.cloud/hzb/bluesky/core/source/bessyii_devices/-/blob/master/bessyii_devices/keithley.py?ref_type=heads
#   ioc:    https://codebase.helmholtz.cloud/hzb/epics/support/keithley/-/blob/main/keithleyApp/Db/Keithley6514Main.template?ref_type=heads

import numpy as np

from ophyd import Component as Cpt
from ophyd import EpicsSignal, EpicsSignalRO, Device, status 
from ophyd.status import SubscriptionStatus
from time import sleep


class Hp3458a(Device):

    def __init__(self, prefix, **kwargs):
        super().__init__(prefix, **kwargs)
        # self.readback.name = self.name # ? 

    # Ophyd device for Hp3458A
    init_cmd    = Cpt(cls = EpicsSignal,    suffix = "RUN",             kind = "omitted")
    abort       = Cpt(cls = EpicsSignal,    suffix = "TARM_HOLD.PROC",  kind = "omitted")
    trigger_cmd = Cpt(cls = EpicsSignal,    suffix = "SAMPLE.PROC",     kind = "omitted")
    readback    = Cpt(cls = EpicsSignalRO,  suffix = "SAMPLE",          kind = "hinted", labels="detectors")
    reset       = Cpt(cls = EpicsSignal,    suffix = "RESET.PROC",      kind = "omitted")
    model       = Cpt(cls = EpicsSignalRO,  suffix = "ID",              kind = "omitted")
    
    # ## -----  error message ------
    err         = Cpt(cls = EpicsSignalRO, suffix = "ERR",      kind = "hinted")
    errmsg      = Cpt(cls = EpicsSignalRO, suffix = "ERRMSG",   kind = "hinted")

    # ## ----- ?write/query? ------
    send        = Cpt(cls = EpicsSignal, suffix = "SEND",   kind = "omitted") 
    query       = Cpt(cls = EpicsSignal, suffix = "QUERY",  kind = "omitted")

    ## -----  configuration ------
    rnge        = Cpt(EpicsSignal,  suffix = "RANGE",  kind = "config", write_pv="RANGE.PROC")
    aper        = Cpt(EpicsSignal,  suffix = "APER",   kind = "config", write_pv="APER.PROC")
    inbuf       = Cpt(EpicsSignal,  suffix = "INBUF",  kind = "config", write_pv="INBUF.PROC")
    arange      = Cpt(EpicsSignal,  suffix = "ARANGE", kind = "config", write_pv="ARANGE.PROC")
    display     = Cpt(EpicsSignal,  suffix = "DISP",   kind = "config", write_pv="DISP.PROC")
    math        = Cpt(EpicsSignal,  suffix = "MATH",   kind = "config", write_pv="MATH.PROC")
    azero       = Cpt(EpicsSignal,  suffix = "AZERO",  kind = "config", write_pv="AZERO.PROC")
    func        = Cpt(EpicsSignal,  suffix = "FUNC",   kind = "config", write_pv="FUNC.PROC")
    tarm        = Cpt(EpicsSignal,  suffix = "TARM",   kind = "config", write_pv="TARM.PROC")
    trig        = Cpt(EpicsSignal,  suffix = "TRIG",   kind = "config", write_pv="TRIG.PROC")
    mem         = Cpt(EpicsSignal,  suffix = "MEM",    kind = "config", write_pv="MEM.PROC")
    mformat     = Cpt(EpicsSignal,  suffix = "MFORMAT",kind = "config", write_pv="MFORMAT.PROC")
    oformat     = Cpt(EpicsSignal,  suffix = "OFORMAT",kind = "config", write_pv="OFORMAT.PROC")
    nrdgs       = Cpt(EpicsSignal,  suffix = "NRDGS",  kind = "config", write_pv="NRDGS.PROC")
    event       = Cpt(EpicsSignal,  suffix = "EVENT",  kind = "config", write_pv="EVENT.PROC")
    timer       = Cpt(EpicsSignal,  suffix = "TIMER",  kind = "config", write_pv="TIMER.PROC")
    mcount      = Cpt(EpicsSignal,  suffix = "MCOUNT", kind = "config", write_pv="MCOUNT.PROC")

    ## -----  read memory ------
    # Note that the array returned is inverted
    rmem        = Cpt(cls = EpicsSignal,  suffix = "RMEM", kind = "config")
    read_array  = Cpt(cls = EpicsSignalRO, suffix = "readMem.VALA", kind = "hinted")

    ## -----  Forward Updates ------
    fwdMain     = Cpt(cls = EpicsSignal, suffix="fwdMain.PROC",    kind = "omitted")
    fwdMem      = Cpt(cls = EpicsSignal, suffix="fwdMem.PROC",     kind = "omitted")
    fwdMeasure  = Cpt(cls = EpicsSignal, suffix="fwdMeasure.PROC", kind = "omitted")

    def read_memory(self):
        self.rmem.put(1)
        sleep(15) #  the process takes around 10 seconds
        return list(self.read_array.get())

    def update_single(self, cpt_proc):   

        def check_value(*, old_value, value, **kwargs):   
            # print("old_value: ", old_value, "value: ", value)
            return (value == old_value)
        
        status = SubscriptionStatus(device = cpt_proc, callback=check_value, run=False)
        # keep trying to process the pv until it is done
        # this is necessary because the device is slow
        while (status.done == False):
            cpt_proc.put(1)
            sleep(1)
            
        print(status)

        return status

    def update_multiple(self, list_cpt_proc=None):

        # update all if not defined
        # it will take around 30s to update all

        if list_cpt_proc is None:

            list_cpt_proc = [self.mcount, self.rnge, self.aper, self.inbuf, 
                             self.arange, self.display, self.math, 
                             self.azero, self.func, self.tarm, 
                             self.trig, self.mem, 
                             self.mformat, self.oformat, 
                             self.nrdgs, self.event, self.timer]
        
        for i in list_cpt_proc:
            status.wait(self.update_single(cpt_proc = i))
    
    # start auto sampling
    def stage(self):
        self.send.put("ARANGE OFF;DISP OFF;MATH OFF;TRIG AUTO;NRDGS AUTO") # trigger as fast as possible
        self.update_multiple(self.rnge, self.func, self.trig, self.event)
        self.init_cmd.put(1) # start
        super().stage()

    # stop sampling
    def unstage(self):
        self.init_cmd.put(0)
        super().unstage()
    
    # # standard routine for multiple measurements
    # std_multiple = "PRESET FAST;\
    #                 APER 8.0E-3;\
    #                 MFORMAT ASCII;\
    #                 MEM FIFO;\
    #                 TIMER 4.70e-5;\
    #                 RANGE 10.0;\
    #                 NRDGS 1000,AUTO"

    # # run multiple measurements
    # def multiple(self, seq = std_multiple):
    #     self.abort.put(1) # ensure measurement is off
    #     self.send.put(seq) # send custom setting sequence:
    #     self.update_settings()
    #     self.send.put("TARM SGL,1")
    #     self.rmem.put(1)
        