# HP3458A EPICS IOC

Welcome to the HP3458A EPICS IOC project! This initiative involves the development of an EPICS (Experimental Physics and Industrial Control System) IOC (Input/Output Controller) for the HP3458A Multimeter. The IOC utilizes the stream-device protocol within the EPICS Control System to establish communication via GPIB with the HP3458A.

Link Documentation:

https://messlabor.pages.hzdr.de/helmholtzcoil/hp3458a-gpib/